package com.ccat.promary;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.sctp.nio.NioSctpServerChannel;

public class SomeServer {
    public static void main(String[] args) throws InterruptedException {

        //用于处理客户端链接请求,将请求发送给childGroup中的eventLoop
        NioEventLoopGroup parentGroup = new NioEventLoopGroup();

        //用于处理客户端请求
        NioEventLoopGroup childGroup = new NioEventLoopGroup();


        try {
            //用户启动Server Channel
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(parentGroup, childGroup)
                    .channel(NioSctpServerChannel.class) //指定使用nio进行通信
                    .childHandler(new SomeChannelInintializer()); //指定childGroup中的eventLoop所绑定的线程所要处理的处理器

            //指定当前服务器所监听的端口号
            //bind()方法的执行是异步的
            //所以要加 sync 变成同步的
            ChannelFuture future = serverBootstrap.bind(9898).sync();
            System.out.println("服务器启动成功，监听的端口号为：8888");
            //关闭channel

            //closeFuture()的执行是异步的
            //当Channel调用了close()方法并关闭成功后才会触发closeFuture()方法的执行
            future.channel().closeFuture().sync();
        } finally {
            //优雅关闭
            parentGroup.shutdownGracefully();
            childGroup.shutdownGracefully();
        }


    }
}
