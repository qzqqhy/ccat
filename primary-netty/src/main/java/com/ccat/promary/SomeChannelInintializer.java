package com.ccat.promary;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

//管道初始化器
public class SomeChannelInintializer extends ChannelInitializer<SocketChannel> {
    //当Channel初始创建完毕后就会触发该方法的执行，用于初始化Channel

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        //从Channel中获取pipeline
        ChannelPipeline pipeline = channel.pipeline();
        //将 HttpServerCodec 处理器放入到pipeline的最后
        //HttpServerCodec 是
        pipeline.addLast("HttpServerCodec", new HttpServerCodec());
        //将自定义的处理器放入到pipeline的最后
        pipeline.addLast(new SomeServerHandler());
    }
}