package com.ccat.promary;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @ClassName：SomeServerHandler
 * @Description： <p>
 *     自定义服务端处理器
 *     需求：用户提交一个请求后，在浏览器就会看到 Hello netty world
 * </p>
 * @Author： - liuxiu
 * @CreatTime：2020-11-16 - 00:02
 * @Modify By：
 * @ModifyTime： 2020-11-16
 * @Modify marker：
 * @version V1.0
*/
public class SomeServerHandler extends ChannelInboundHandlerAdapter {

    /**
    * @description: 当 channel中 有来自于客户端的数据时就会触发该方法执行
    * @param ctx 上下文对象
    * @param msg 来自客户端的数据
    * @return
    * @throws
    * @author liuxiu
    * @date 2020-11-16 00:04
    */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("msg:" + msg.getClass());
        System.out.println("客户端地址:" + ctx.channel().remoteAddress());
    }

    /**
    * @description: 当channel 的数据在处理过程中出现异常时会触发该方法的执行
    * @param ctx 上下文对象
    * @param cause 发生的异常对象
    * @return
    * @throws
    * @author liuxiu
    * @date 2020-11-16 00:14
    */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        //关闭channel
        ctx.close();
    }
}
