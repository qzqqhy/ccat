package com.ccat.provider.repository;

import com.ccat.provider.bean.Depart;
import org.springframework.data.jpa.repository.JpaRepository;

//第一个泛型，是操作的对象 depart
//第二个泛型是，操作对象的id类型
public interface DepartRepository extends JpaRepository<Depart,Integer> {
}
