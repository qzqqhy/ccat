package com.ccat.provider.service.impl;

import com.ccat.provider.bean.Depart;
import com.ccat.provider.repository.DepartRepository;
import com.ccat.provider.service.DepartService;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartServiceImpl implements DepartService {


    @Autowired
    DepartRepository departRepository;

    @Override
    public boolean save(Depart depart) {
        //depart id is null: run insert
        //depart id not null and database exsit: run update
        //depart id not null and database not exsit: run save, this id no database id ,is celue gen id
        Depart save = departRepository.save(depart);

        return save != null;
    }

    @Override
    public boolean remove(Integer id) {
        if(departRepository.existsById(id)){
            //数据库不存在id的话 delete会抛异常
            departRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean modify(Depart depart) {
        Depart save = departRepository.save(depart);

        return save != null;
    }

    @Override
    public Depart query(Integer id) {
        if(departRepository.existsById(id)){
            //数据库不存在id的话 delete会抛异常
            return departRepository.getOne(id);
        }
        return null;
    }

    @Override
    public List<Depart> queryList(Depart depart) {
        return departRepository.findAll();
    }
}
