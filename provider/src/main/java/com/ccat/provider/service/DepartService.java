package com.ccat.provider.service;

import com.ccat.provider.bean.Depart;

import java.util.List;

public interface DepartService {
    boolean save(Depart depart);
    boolean remove(Integer id);
    boolean modify(Depart depart);
    Depart query(Integer id);
    List<Depart> queryList(Depart depart);
}
