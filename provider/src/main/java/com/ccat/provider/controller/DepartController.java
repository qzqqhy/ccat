package com.ccat.provider.controller;

import com.alibaba.fastjson.JSONObject;
import com.ccat.provider.bean.Depart;
import com.ccat.provider.service.DepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/provider/depart")
public class DepartController {
    @Autowired
    DepartService departService;

    /***
     * 声明服务发现客户端
     */
    @Autowired
    DiscoveryClient client;


    @PostMapping("/")
    public boolean saveHandler(@RequestBody Depart depart) {
        return departService.save(depart);
    }

    @DeleteMapping("/{id}")
    public boolean delHandler(@PathVariable("id") Integer id) {
        return departService.remove(id);
    }

    @PutMapping("/")
    public boolean updateHandler(@RequestBody Depart depart) {
        return departService.modify(depart);
    }

    @GetMapping("/{id}")
    public Depart getHandler(@PathVariable("id") Integer id) {
        return departService.query(id);
    }

    @GetMapping("/")
    public List<Depart> listHandler() {
        return departService.queryList(null);
    }

    @GetMapping("/discovery")
    public Map<String, String> discoveryHandler() {
        Map<String, String> res = new LinkedHashMap<>();
        List<String> services = client.getServices();
        res.put("services", JSONObject.toJSONString(services));

        for (String name : services) {
            Map<String, String> res_1 = new LinkedHashMap<>();
            //获取当前遍历微服务名称的所有提供者主机
            List<ServiceInstance> instances = client.getInstances(name);
//            res_1.put(name,JSONObject.toJSONString(instances));
            for (ServiceInstance instance : instances) {
                //获取提供者的唯一标示
                String serviceId = instance.getServiceId();
                res.put(name + "serviceId", serviceId);

                String instanceId = instance.getInstanceId();
                res.put(name + "instanceId", instanceId);

                //获取当前服务提供者的主机host
                String host = instance.getHost();
                res.put(name + "host", host);

                //微服务的元数据信息
                Map<String, String> metadata = instance.getMetadata();
                res.put(name + "metadata", JSONObject.toJSONString(metadata));
            }
        }
        return res;
    }
}
