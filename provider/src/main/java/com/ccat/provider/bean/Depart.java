package com.ccat.provider.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity(name = "t_depart")
//HttpMessageConverter jackson -> 完成java 对象与JSON数据间的转换工作
//jpa 默认实现是 Hibernate,而Hibernate 默认对于对象的查询是基于延迟加载
@JsonIgnoreProperties({"hibernateLazyInitializer","hanlder","fieldHandler"})
public class Depart {
    @Id //表示当前属性为自动建表的主键
    @GeneratedValue(strategy = GenerationType.IDENTITY) //主键自增
    private Integer id;
    private String name;

}
