package com.ccat.consumer.controller;

import com.ccat.consumer.bean.Depart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/consumer/depart")
public class SomeController {

    @Autowired
    private RestTemplate restTemplate;

    //直连提供者
    // private static final String PATH = "http://localhost:8081/provider/depart/";
    //使用微服务名称来从eureka server 查找提供者
    private static final String PATH = "http://ccat-provider-depart/provider/depart/";

    @PostMapping("/")
    public boolean saveHandler(@RequestBody Depart depart) {

        Boolean aBoolean = restTemplate.postForObject(PATH, depart, Boolean.class);

        return aBoolean;
    }

    @DeleteMapping("/{id}")
    public void delHandler(@PathVariable("id") Integer id) {
        restTemplate.delete(PATH + id);
    }

    @PutMapping("/")
    public void updateHandler(@RequestBody Depart depart) throws RestClientException {
        restTemplate.put(PATH, depart);
    }

    @GetMapping("/{id}")
    public Depart getHandler(@PathVariable("id") Integer id) {
        return restTemplate.getForObject(PATH + id, Depart.class);
    }

    @GetMapping("/")
    public List<Depart> listHandler() {
        return restTemplate.getForObject(PATH, List.class);
    }

}