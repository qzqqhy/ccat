package com.ccat.consumer.bean;

import lombok.Data;

@Data
public class Depart {
    private Integer id;
    private String name;
}